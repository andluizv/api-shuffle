from app.configs.database import db
from sqlalchemy import Column, Integer, ForeignKey


character_items = db.Table('characters_items',
    Column('id', Integer, primary_key=True),
    Column('character_id', Integer, ForeignKey("characters.id", ondelete="cascade"), nullable=False),
    Column('item_id', Integer, ForeignKey('items.id'), nullable=False))
