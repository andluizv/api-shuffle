from app.configs.database import db
from sqlalchemy import Column, Integer, String


class TypeModel(db.Model):
    __tablename__ = 'types'

    id = Column(Integer, primary_key=True)
    name = Column(String(16), unique=True, nullable=False)
