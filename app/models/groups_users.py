from app.configs.database import db
from sqlalchemy import Column, Integer, ForeignKey


groups_users = db.Table('groups_users',
    Column('id', Integer, primary_key=True),
    Column('user_id', Integer, ForeignKey('users.id'), nullable=False),
    Column('group_id', Integer, ForeignKey("groups.id"), nullable=False))
