from sqlalchemy.orm import relationship, validates
from app.configs.database import db 
from sqlalchemy import Column, Integer, String, Float, ForeignKey
from dataclasses import dataclass
from app.exceptions import WrongFieldsError, InvalidFieldTypeError, NotAcceptedFieldError, TypeNotFoundError


@dataclass
class ItemModel(db.Model):
    id: int
    name: str
    weight: float
    description: str
    damage: str
    max_ammo: int
    ammo: int
    attacks: str
    range: int
    defect: int
    area: int
    type_id: int

    __tablename__ = 'items'

    id = Column(Integer, primary_key=True)
    name = Column(String(16), nullable=False)
    weight = Column(Float)
    description = Column(String(128), nullable=False)
    damage = Column(String)
    max_ammo = Column(Integer)
    ammo = Column(Integer)
    attacks = Column(String)
    range = Column(Integer)
    defect = Column(Integer)
    area = Column(Integer)
    type_id = Column(Integer, ForeignKey('types.id'), nullable=False)

    type = relationship('TypeModel', backref='type', uselist=False)
    char = relationship('CharacterModel', secondary='characters_items', backref='items')


    @validates('name', 'weight', 'description', 'damage', 'max_ammo', 'ammo', 'attacks', 'range', 'defect', 'area', 'type_id')
    def validate_fields(self, key: str, value: any) -> any:
        if key in ['name', 'damage', 'attacks']:
            if type(value) != str:
                raise InvalidFieldTypeError(key, value, 'string')
            else:
                return value.title()
        if key == 'description':
            if type(value) != str:
                raise InvalidFieldTypeError(key, value, 'string')
            else:
                return value.capitalize()
        if key in ['max_ammo', 'ammo', 'range', 'defect', 'area', 'type_id'] and type(value) != int:
            raise InvalidFieldTypeError(key, value, 'integer')

        if key == 'weight':
            if type(value) != int and type(value) != float:
                raise InvalidFieldTypeError(key, value, 'integer or float')

            if type(value) == int:
                return float(value)

        if key == "type_id":
            if value < 1 and value > 4:
                raise TypeNotFoundError
        return value


    @staticmethod
    def verify_fields_create(request: dict) -> None:
        required_fields = ['name', 'description', 'type_id']
        not_required_fields = ['weight', 'damage', 'max_ammo', 'ammo', 'attacks', 'range', 'defect', 'area']

        if list(request.keys()) != required_fields:
            missing_fields = [field for field in required_fields if field not in request.keys()]
            unsolicited_fields = [field for field in request.keys() if field not in required_fields and field not in not_required_fields]
            if missing_fields or unsolicited_fields:
                raise WrongFieldsError(missing_fields, unsolicited_fields)


    @staticmethod
    def verify_fields_update(request: dict) -> None:
        accepted_fields = ["name", "weight", "description", "damage", "max_ammo", "ammo", "attacks", "range", "defect", "area", "type_id"]

        for field in request.keys():
            if field not in accepted_fields:
                raise NotAcceptedFieldError(field, accepted_fields)
