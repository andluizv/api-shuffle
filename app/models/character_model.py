from app.configs.database import db
from sqlalchemy import Column, Integer, String, Float, ForeignKey
from sqlalchemy.orm import relationship, validates
from dataclasses import dataclass
from app.exceptions import InvalidFieldTypeError, WrongFieldsError, NotAcceptedFieldError


@dataclass
class CharacterModel(db.Model):
    id: int
    name: str
    age: int
    gender: str
    occupation: str
    max_life: int
    life: int
    max_mana: int
    mana: int
    height: float
    body: int
    moviment: int
    image_url: str
    attributes: list
    skills: list

    __tablename__ = 'characters'

    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False)
    age = Column(Integer)
    gender = Column(String(1))
    occupation = Column(String(32))
    max_life = Column(Integer, nullable=False)
    life = Column(Integer, nullable=False)
    max_mana = Column(Integer, nullable=False)
    mana = Column(Integer, nullable=False)
    height = Column(Float)
    body = Column(Integer)
    moviment = Column(Integer)
    image_url = Column(String, default='https://i.imgur.com/hFl5p9n.png')
    user_id = Column(Integer, ForeignKey('users.id', ondelete='cascade'), nullable=False)
    attributes_id = Column(Integer, ForeignKey('attributes.id', ondelete='cascade'), unique=True, nullable=False)

    attributes = relationship('AttributeModel')
    skills = relationship('CharactersSkills')


    @validates('name', 'age', 'gender', 'occupation', 'max_life', 'life', 'max_mana', 'mana', 'height', 'body', 'moviment')
    def validate_fields(self, key: str, value: any) -> any:
        if key in ['name', 'gender', 'occupation']:
            if type(value) != str:
                raise InvalidFieldTypeError(key, value, 'string')
            else:
                return value.title()

        if key in ['age', 'max_life', 'life', 'max_mana', 'mana', 'body', 'moviment'] and type(value) != int:
            raise InvalidFieldTypeError(key, value, 'integer')

        if key == 'height':
            if type(value) != int and type(value) != float:
                raise InvalidFieldTypeError(key, value, 'integer or float')

            if type(value) == int:
                return float(value)

        return value


    @staticmethod
    def verify_fields_create(request: dict) -> None:
        required_fields = ['name', 'max_life', 'life', 'max_mana', 'mana']
        not_required_fields = ['age', 'gender', 'occupation', 'height', 'body', 'moviment']

        if list(request.keys()) != required_fields:
            missing_fields = [field for field in required_fields if field not in request.keys()]
            unsolicited_fields = [field for field in request.keys() if field not in required_fields and field not in not_required_fields]
            if missing_fields or unsolicited_fields:
                raise WrongFieldsError(missing_fields, unsolicited_fields)


    @staticmethod
    def verify_fields_update(request: dict) -> None:
        accepted_fields = ['name', 'age', 'gender', 'occupation', 'max_life', 'life', 'max_mana', 'mana', 'height', 'body', 'moviment', 'image_url']

        for field in request.keys():
            if field not in accepted_fields:
                raise NotAcceptedFieldError(field, accepted_fields)
