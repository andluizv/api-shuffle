from flask import Blueprint
from app.controllers.attributes_controller import update_attributes


bp = Blueprint('bp_attributes', __name__, url_prefix='/attribute')


bp.patch('/<int:char_id>')(update_attributes)
