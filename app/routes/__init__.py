from flask import Flask
from app.routes.users_blueprint import bp as bp_users
from app.routes.character_blueprint import bp as bp_characters
from app.routes.attributes_blueprint import bp as bp_attributes
from app.routes.skills_blueprint import bp as bp_skills
from app.routes.items_blueprint import bp as bp_items
from app.routes.groups_blueprint import bp as bp_groups


def init_app(app: Flask) -> None:
    app.register_blueprint(bp_users)
    app.register_blueprint(bp_characters)
    app.register_blueprint(bp_attributes)
    app.register_blueprint(bp_skills)
    app.register_blueprint(bp_items)
    app.register_blueprint(bp_groups)
