from functools import wraps
from flask_jwt_extended import get_jwt_identity
from app.models.users_model import UserModel


def validate_access(func):
    @wraps(func)
    def decorator(*args, **kwargs):
        user = get_jwt_identity()
        user_found = UserModel.query.get(user['id'])
        if not user_found:
            return {'error': 'token inválido'}, 401
        return func(*args, **kwargs)
    return decorator
