from flask import current_app, request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from sqlalchemy import and_
from dataclasses import asdict
from app.controllers import validate_access
from app.models.character_model import CharacterModel
from app.models.attributes_model import AttributeModel
from app.models.type_model import TypeModel
from werkzeug.exceptions import NotFound
from app.exceptions import (
    NoRequisitionBodyError,
    WrongFieldsError,
    NotAcceptedFieldError,
    InvalidFieldTypeError
)


@jwt_required()
@validate_access
def create_character():
    session = current_app.db.session

    data: dict = request.get_json()

    try:
        if not data:
            raise NoRequisitionBodyError

        CharacterModel.verify_fields_create(data)

        user = get_jwt_identity()

        data['user_id'] = user['id']
        data['attributes_id'] = AttributeModel.create_attributes(session)

        new_character = CharacterModel(**data)

        session.add(new_character)
        session.commit()

        serializer = {
            'name': new_character.name,
            'age': new_character.age,
            'gender': new_character.gender,
            'occupation': new_character.occupation,
            'max_life': new_character.max_life,
            'life': new_character.life,
            'max_mana': new_character.max_mana,
            'mana': new_character.mana,
            'height': new_character.height,
            'body': new_character.body,
            'moviment': new_character.moviment
        }

        return jsonify(serializer), 201

    except (NoRequisitionBodyError, WrongFieldsError, InvalidFieldTypeError) as err:
        if data and 'attributes_id' in data:
            AttributeModel.delete_attributes(session, data['attributes_id'])

        return jsonify(err.message), 400


@jwt_required()
@validate_access
def read_all_characters():
    user = get_jwt_identity()

    characters = CharacterModel.query.filter_by(user_id=user['id']).all()

    serialize = [
        {
            'id': character.id,
            'name': character.name,
            'max_life': character.max_life,
            'life': character.life,
            'max_mana': character.max_mana,
            'mana': character.mana,
            'image_url': character.image_url
        } for character in characters
    ]

    return jsonify(serialize), 200


@jwt_required()
@validate_access
def read_character(char_id: int):
    try:
        character_query = CharacterModel.query.get_or_404(char_id)
        character = asdict(character_query)

        attributes: dict = character.pop('attributes')
        character['attributes'] = [
            {
                'name': key,
                'value': value
            } for key, value in attributes.items()
        ]

        skills = character.pop('skills')
        character['skills'] = [
            {
                'id': skill['skill_infos']['id'],
                'name': skill['skill_infos']['name'],
                'value': skill['value']
            } for skill in skills
        ]
        
        character['items'] = list()
        for item in character_query.items:
            new_item = asdict(item)
            type_id = new_item.pop('type_id')
            type_name = TypeModel.query.get(type_id)
            new_item['type'] = type_name.name
            character['items'].append(new_item)

        return jsonify(character), 200

    except NotFound:
        return jsonify({'error': 'character not found'}), 404


@jwt_required()
@validate_access
def update_character(char_id: int):
    session = current_app.db.session

    data: dict = request.get_json()

    try:
        if not data:
            raise NoRequisitionBodyError

        CharacterModel.verify_fields_update(data)

        user = get_jwt_identity()

        character = CharacterModel.query.filter(and_(CharacterModel.user_id == user['id'], CharacterModel.id == char_id)).first_or_404()

        for key, value in data.items():
            setattr(character, key, value)

        session.add(character)
        session.commit()

        serializer = {
            'name': character.name,
            'age': character.age,
            'gender': character.gender,
            'occupation': character.occupation,
            'max_life': character.max_life,
            'life': character.life,
            'max_mana': character.max_mana,
            'mana': character.mana,
            'height': character.height,
            'body': character.body,
            'moviment': character.moviment,
            'image_url': character.image_url
        }

        return jsonify(serializer), 200

    except NotFound:
        return jsonify({'error': 'character not found'}), 404

    except (NoRequisitionBodyError, NotAcceptedFieldError, InvalidFieldTypeError) as err:
        return jsonify(err.message), 400


@jwt_required()
@validate_access
def delete_character(char_id: int):
    session = current_app.db.session

    try:
        user = get_jwt_identity()

        character = CharacterModel.query.filter(and_(CharacterModel.user_id == user['id'], CharacterModel.id == char_id)).first_or_404()

        character_attributes = AttributeModel.query.get(character.attributes.id)

        session.delete(character)
        session.delete(character_attributes)
        session.commit()

        return jsonify({'message': f'character \'{character.name}\' successfully deleted'}), 200

    except NotFound:
        return jsonify({'error': 'character not found'}), 404
