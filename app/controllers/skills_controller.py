from flask import current_app, request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from sqlalchemy import and_
from dataclasses import asdict
from app.controllers import validate_access
from app.models.skills_model import SkillModel
from app.models.character_model import CharacterModel
from app.models.characters_skills_model import CharactersSkills
from werkzeug.exceptions import NotFound
from app.exceptions import (
    NoRequisitionBodyError,
    WrongFieldsError,
    NotAcceptedFieldError,
    InvalidFieldTypeError
)


@jwt_required()
@validate_access
def create_skill(char_id: int):
    session = current_app.db.session

    data: dict = request.get_json()

    try:
        if not data:
            raise NoRequisitionBodyError

        SkillModel.verify_fields_create(data)

        if type(data['name']) != str:
            raise InvalidFieldTypeError('name', data['name'], 'string')
        else:
            data['name'] = data['name'].title()

        user = get_jwt_identity()

        character = CharacterModel.query.filter(and_(CharacterModel.user_id == user['id'], CharacterModel.id == char_id)).first_or_404()

        skill = SkillModel.query.filter_by(name=data['name']).first()
        if not skill:
            skill = SkillModel(**data)

        character.skills.append(CharactersSkills(skill=skill))
        session.commit()

        character = asdict(character)
        skills = character.pop('skills')
        character['skills'] = [
            {
                'name': skill['skill_infos']['name'],
                'value': skill['value']
            } for skill in skills
        ]

        return jsonify({'character': character['name'], 'skills': character['skills']}), 201

    except NotFound:
        return jsonify({'error': 'character not found'}), 404

    except (NoRequisitionBodyError, WrongFieldsError, InvalidFieldTypeError) as err:
        return jsonify(err.message), 400


@jwt_required()
@validate_access
def update_skill(skill_id: int):
    session = current_app.db.session

    data: dict = request.get_json()

    try:
        if not data:
            raise NoRequisitionBodyError

        char_id = request.args.get('char_id')
        if not char_id:
            return jsonify({'error': 'query param needed: char_id'}), 400

        accepted_fields = ['value']
        for field in data.keys():
            if field not in accepted_fields:
                raise NotAcceptedFieldError(field, accepted_fields)

        if type(data['value']) != int:
            raise InvalidFieldTypeError('value', data['value'], 'integer')

        user = get_jwt_identity()

        character = CharacterModel.query.filter(and_(CharacterModel.user_id == user['id'], CharacterModel.id == char_id)).first_or_404()

        skill = SkillModel.query.get_or_404(skill_id)

        skill_pivo = CharactersSkills.query.filter_by(character_id=character.id, skill_id=skill.id).first()
        if not skill_pivo:
            return jsonify({'error': f'{character.name} doesn\'t have skill {skill.name}'}), 400

        skill_pivo.value = data['value']

        session.commit()

        return jsonify({'character': character.name, 'skill': {'name': skill.name, 'value': skill_pivo.value}}), 200

    except NotFound:
        return jsonify({'error': 'character or skill not found'}), 404

    except (NoRequisitionBodyError, NotAcceptedFieldError, InvalidFieldTypeError) as err:
        return jsonify(err.message), 400


@jwt_required()
@validate_access
def delete_skill(skill_id: int):
    session = current_app.db.session

    try:
        char_id = request.args.get('char_id')
        if not char_id:
            return jsonify({'error': 'query param needed: char_id'}), 400

        user = get_jwt_identity()

        character = CharacterModel.query.filter(and_(CharacterModel.user_id == user['id'], CharacterModel.id == char_id)).first_or_404()
        
        skill = SkillModel.query.get_or_404(skill_id)

        deleted_skill = CharactersSkills.query.filter(and_(CharactersSkills.character_id == character.id, CharactersSkills.skill_id == skill.id)).first()
        if not deleted_skill:
            return jsonify({'error': f'{character.name} doesn\'t have skill {skill.name}'}), 400

        session.delete(deleted_skill)
        session.commit()

        return jsonify({'message': f'skill {skill.name} has been removed from {character.name}'}), 200

    except NotFound:
        return jsonify({'error': 'character or skill not found'}), 404
