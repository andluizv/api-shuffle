from flask import current_app, request, jsonify
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity
from datetime import timedelta
from app.controllers import validate_access
from app.models.users_model import UserModel
from sqlalchemy.exc import IntegrityError
from psycopg2.errors import UniqueViolation
from werkzeug.exceptions import NotFound
from app.exceptions import (
    NoRequisitionBodyError,
    InvalidFieldTypeError,
    WrongFieldsError,
    NotAcceptedFieldError,
    LoginError
)


def signup():
    session = current_app.db.session

    data: dict = request.get_json()

    try:
        if not data:
            raise NoRequisitionBodyError

        required_fields = ['name', 'email', 'password']
        UserModel.verify_fields_create(data, required_fields)

        new_user = UserModel(**data)

        session.add(new_user)
        session.commit()

        return jsonify({'access_token': create_access_token(new_user, expires_delta=timedelta(days=1))}), 201

    except IntegrityError as err:
        if type(err.orig) == UniqueViolation:
            return jsonify({'error': 'email already registered'}), 409

    except (NoRequisitionBodyError, WrongFieldsError, InvalidFieldTypeError) as err:
        return err.message, 400


def login():
    data = request.get_json()

    try:
        if not data:
            raise NoRequisitionBodyError

        required_fields = ['email', 'password']
        UserModel.verify_fields_create(data, required_fields)

        for key, value in data.items():
            if type(value) != str:
                raise InvalidFieldTypeError(key, value, 'string')

        user: UserModel = UserModel.query.filter_by(email=data['email']).first_or_404()

        user.verify_password(data['password'])

        return jsonify({'access_token': create_access_token(user, expires_delta=timedelta(days=1))}), 200

    except NotFound:
        return jsonify({'error': 'email not found in our system'}), 404

    except LoginError as err:
        return jsonify(err.message), 401

    except (NoRequisitionBodyError, WrongFieldsError, InvalidFieldTypeError) as err:
        return jsonify(err.message), 400


@jwt_required()
@validate_access
def read_user():
    user = get_jwt_identity()

    read_user = UserModel.query.get(user['id'])

    return jsonify(read_user), 200


@jwt_required()
@validate_access
def update_user():
    session = current_app.db.session

    data: dict = request.get_json()

    try:
        if not data:
            raise NoRequisitionBodyError

        UserModel.verify_fields_update(data)

        user = get_jwt_identity()

        updated_user = UserModel.query.get(user['id'])

        for key, value in data.items():
            setattr(updated_user, key, value)

        session.add(updated_user)
        session.commit()

        return {'message': 'successfully changed data'}, 200

    except (NoRequisitionBodyError, NotAcceptedFieldError, InvalidFieldTypeError) as err:
        return jsonify(err.message), 400


@jwt_required()
@validate_access
def delete_user():
    session = current_app.db.session

    user = get_jwt_identity()

    deleted_user = UserModel.query.get(user['id'])

    session.delete(deleted_user)
    session.commit()

    return {'message': f'user \'{deleted_user.name}\' successfully deleted'}, 200
