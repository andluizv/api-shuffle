# API Shuffle - RPG Character Sheets

## Rota User

### POST/user/signup

Cria um usuario

Requisition body:

```json
{
  "name": "nome",
  "email": "example@mail.com",
  "password": "1234"
}
```

Response:

```json
{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTYzOTQxNzMyMiwianRpIjoiMDg1OTViODYtNDkxOS00N2ZkLWJkM2YtMjBkMTI4NjJkNWJkIiwidHlwZSI6ashfjghufhufsdYiI6eyJpZCI6MiwibmFtZSI6Ik1hdGVtYXRpY28iLCJlbWFpbCI6ImFiY0BnbWFpbC5jb20ifSwibmJmIjoxNjM5NDE3MzIyLCJleHAiOjE2Mzk1MDM3MjJ9.jbudjkgujtugjeju-pxph9wj7kmbDY-3ecdsadsadsa"
}
```

### POST/user/login

Retorna o token de login do usuário

Obs: O token ira expirar em até 24 horas.

Requisition body:

```json
{
  "email": "example@gmail.com",
  "password": "1234"
}
```

Response:

```json
{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTYzOTQxNzMyMiwianRpIjoiMDg1OTViODYtNDkxOS00N2ZkLWJkM2YtMjBkMTI4NjJkNWJkIiwidHlwZSI6ashfjghufhufsdYiI6eyJpZCI6MiwibmFtZSI6Ik1hdGVtYXRpY28iLCJlbWFpbCI6ImFiY0BnbWFpbC5jb20ifSwibmJmIjoxNjM5NDE3MzIyLCJleHAiOjE2Mzk1MDM3MjJ9.jbudjkgujtugjeju-pxph9wj7kmbDY-3ecdsadsadsa"
}
```

### GET/user

Está rota Precisa de um token de autorização

Retorna as informações do usuário

No Requisition body

Response:

```json
{
  "id": 0,
  "name": "nome",
  "email": "example@gmail.com"
}
```

### PATCH/user

Atualiza os dados do usuário

# Examples

Requisition body:

```json
{
  "name": "name",
  "password": "1234"
}
```

Response:

```json
{
  "message": "successfully changed data"
}
```

### DELETE/user

No Requisition body

Response:

```json
{
  "message": "user 'Name' successfully deleted"
}
```

## Rota Character

### POST/character

Cria um personagem para o usuário

Requisition body:

```json
{
  "name": "Name Surname",
  "age": 22,
  "gender": "m",
  "occupation": "merchant",
  "max_life": 999,
  "life": 999,
  "max_mana": 1,
  "mana": 1,
  "height": 1.7,
  "body": 0,
  "moviment": 8
}
```

Response:

```json
{
  "name": "Name Surname",
  "age": 22,
  "gender": "M",
  "occupation": "merchant",
  "max_life": 999,
  "life": 999,
  "max_mana": 1,
  "mana": 1,
  "height": 1.7,
  "body": 0,
  "moviment": 8
}
```

### GET/character

No Requisition body

Response:

```json
[
  {
    "id": 1,
    "name": "Name Surname",
    "max_life": 999,
    "life": 999,
    "max_mana": 1,
    "mana": 1,
    "image_url": "https://path/image.png"
  },
  {
    "id": 2,
    "name": "Name Surname",
    "max_life": 100,
    "life": 88,
    "max_mana": 21,
    "mana": 10,
    "image_url": "https://path/image.png"
  }
]
```

### GET/character/char_id

Retorna todos dados do personagem especificado

No Requisition body

Response:

```json
{
  "name": "Name Surname",
  "age": 23,
  "gender": "F",
  "occupation": "merchant",
  "max_life": 11,
  "life": 1,
  "max_mana": 12,
  "mana": 12,
  "height": 1.7,
  "body": 0,
  "moviment": 8,
  "attributes": [
    {
      "name": "constitution",
      "value": 0
    },
    {
      "name": "power",
      "value": 0
    },
    {
      "name": "strength",
      "value": 0
    },
    {
      "name": "dexterity",
      "value": 0
    },
    {
      "name": "intelligence",
      "value": 0
    },
    {
      "name": "luck",
      "value": 0
    },
    {
      "name": "appearance",
      "value": 0
    },
    {
      "name": "education",
      "value": 0
    }
  ],
  "skills": [],
  "items": []
}
```

### PATCH/character/char_id

Atualiza informações do personagem específico

Requisition body:

```json
{
  "name": "Sir weinz",
  "age": 2222,
  "gender": "m",
  "occupation": "Paladin",
  "max_life": 999,
  "life": 999,
  "max_mana": 1,
  "mana": 1,
  "height": 1.7,
  "body": 0,
  "moviment": 8
}
```

Response:

```json
{
  "name": "Sir Weinz",
  "age": 2222,
  "gender": "M",
  "occupation": "Paladin",
  "max_life": 999,
  "life": 999,
  "max_mana": 1,
  "mana": 1,
  "height": 1.7,
  "body": 0,
  "moviment": 8,
  "attributes": {
    "constitution": 0,
    "power": 0,
    "strength": 0,
    "dexterity": 0,
    "intelligence": 0,
    "luck": 0,
    "appearance": 0,
    "education": 0
  },
  "skills": []
}
```

### DELETE/character/char_id

No Requisition body:

Response:

```json
{
  "message": "character 'Sir weinz' successfully deleted"
}
```

## Rota Attribute

### PATCH/attribute/char_id

Atualiza os atributos do personagem especificado

Requisition Body:

```json
{
  "constitution": 10,
  "power": 6,
  "strength": 4,
  "dexterity": 4,
  "intelligence": 4,
  "luck": 8,
  "appearance": 4,
  "education": 4
}
```

Response:

```json
{
  "character": "Sir Weinz",
  "atributes": {
    "constitution": 10,
    "power": 6,
    "strength": 4,
    "dexterity": 4,
    "intelligence": 4,
    "luck": 8,
    "appearance": 4,
    "education": 4
  }
}
```

## Rota Skill

### POST/skill/char_id

Requisition body:

```json
{
  "name": "Self-healing"
}
```

Response:

```json
{
  "Character": "Sir Weinz",
  "Skills": [
    {
      "value": 0,
      "name": "Self-Healing",
      "id": 1
    },
    {
      "value": 0,
      "name": "Self-Healing",
      "id": 1
    }
  ]
}
```

### PATCH/skill/char_id

Atualiza informações da skill especificada

Requisition body:

```json
{
  "value": 100
}
```

Response:

```json
{
  "character": "Sir Weinz",
  "skill": "Self-Healing",
  "value": 100
}
```

### DELETE/skill/char_id

Deleta a skill especificada

No Requisition body:

Response:

```json
{
  "message": "skill Self-Healing' has been removed from Sir Weinz"
}
```

## Rota Item

### POST/item/char_id

Cria um item

Requisition body:

```json
{
  "name": "knife1",
  "weight": 0.5,
  "description": "very sharp",
  "damage": "50",
  "max_ammo": 0,
  "ammo": 0,
  "attacks": "1",
  "range": 1,
  "defect": 0,
  "area": 3,
  "type_id": 1
}
```

Response:

```json
{
  "id": 1,
  "name": "Knife1",
  "weight": 0.5,
  "description": "Very Sharp",
  "damage": "50",
  "max_ammo": 0,
  "ammo": 0,
  "attacks": "1",
  "range": 1,
  "defect": 0,
  "area": 3,
  "type": "standart"
}
```

### GET/item/types

Retorna os tipos disponíveis

No Requisition body:

Response:

```json
[
  {
    "id": 1,
    "name": "standart"
  },
  {
    "id": 2,
    "name": "key"
  },
  {
    "id": 3,
    "name": "combat"
  },
  {
    "id": 4,
    "name": "medicine"
  }
]
```

### PATCH/item/item_id

Atualiza informação do item especificado

Requisition body:

```json
{
  "name": "hammer",
  "weight": 0.5,
  "description": "very heavy",
  "damage": "50",
  "max_ammo": 0,
  "ammo": 0,
  "attacks": "1",
  "range": 1,
  "defect": 0,
  "area": 3,
  "type_id": 3
}
```

Response:

```json
{
  "id": 1,
  "name": "Hammer",
  "weight": 0.5,
  "description": "Very Heavy",
  "damage": "50",
  "max_ammo": 0,
  "ammo": 0,
  "attacks": "1",
  "range": 1,
  "defect": 0,
  "area": 3,
  "type": "combat"
}
```

### DELETE/item/item_id?char_id

Deleta o item especificado

No Requisition body:

Response:

```json
{
  "message": "character \"Hammer\" successfully deleted"
}
```

## Rota Group

### POST/group

Criar um grupo

Requisition body:

```json
{
  "name": "Grupo fullstack"
}
```

Response:

```json
{
  "id": 1,
  "name": "Grupo fullstack",
  "master_id": null
}
```

### GET/group

Retorna todos grupos, com id, nome, nº de membros, nome do mestre

No Requisition body

Response:

```json
[
  {
    "id": 1,
    "name": "Grupo fullstack",
    "master": null,
    "members": [
      {
        "id": 1,
        "name": "Name",
        "email": "example@gmail.com"
      }
    ]
  }
]
```

### GET/group/group_id

Retorna todas informações do grupo (id, nome, mestre, nome dos membros com email)

No Requisition body

Response:

```json
[
  {
    "id": 1,
    "name": "Grupo fullstack",
    "master": null,
    "members": [
      {
        "id": 1,
        "name": "Name",
        "email": "example@gmail.com"
      }
    ]
  }
]
```

### PATCH/group/group_id

Atualiza dados do grupo especificado

Requisition body:

```json
{
  "name": "Grupo junior",
  "master_id": 3
}
```

Response:

```json
{
  "id": 1,
  "name": "Grupo Junior",
  "master_id": 3
}
```

### POST/group/group_id?user

Adiciona um usuário específico ao grupo especificado

No Requisition body:

Response:

```json
{
  "message": "usuario entrou no grupo Grupo junior"
}
```

### POST/group/group_id?char

Adiciona personagem ao grupo

No Requisition body:

Response:

```json
{
  "message": "personagem entrou no grupo Grupo junior"
}
```

### DELETE/group/group_id?user

Remove o usuario do grupo

No Requisition body:

Response:

```json
{
  "message": "usuario foi removido do grupo Grupo junior"
}
```

### DELETE/group/group_id?char

Remove o personagem do usuario do grupo

No Requisition body:

Response:

```json
{
  "message": "personagem foi removido do grupo Grupo junior"
}
```

### DELETE/group/group_id

Deleta o grupo especificado pelo group_id

No Requisition body:

Response:

```json
{
  "message": "Grupo 'nome do grupo' deletado com sucesso"
}
```
